﻿using System;

namespace week_3_exercise_20
{
    class Program
    {
        static void Main(string[] args)
        {
            var usrNum1 = 0;
            var usrNum2 = 0;
            var usrPass = "";
            var usrOper = "";
            var usrExit = false;


            Console.WriteLine("Please enter a first number:");
            usrNum1 = int.Parse(Console.ReadLine());

            Console.WriteLine("Now enter a second number:");
            usrNum2 = int.Parse(Console.ReadLine());

            Console.WriteLine("Please enter a password");
            usrPass = Console.ReadLine();


            while(!usrExit)
            {
                Console.Clear();
                Console.WriteLine("Which operator would you like to look at?");
                Console.WriteLine("Type -compare for the Comparison operator.");
                Console.WriteLine("Type -length for the Length operator.");
                Console.WriteLine("Type -replace for the Replacement operator.");
                Console.WriteLine("Type -exit to exit the application.");
                Console.WriteLine($"Num1 = {usrNum1}");
                Console.WriteLine($"Num2 = {usrNum2}");
                    usrOper = Console.ReadLine().ToLower();

                if(usrOper == "-compare")
                {
                    Console.Clear();
                    Console.WriteLine("Compare:");
                    Console.WriteLine();
                    Console.WriteLine($"Num1 compared to Num2 = {usrNum1.CompareTo(usrNum2)}");
                    Console.ReadKey();
                }
                else if(usrOper == "-length")
                {
                    Console.Clear();
                    Console.WriteLine("Length:");
                    Console.WriteLine();
                    Console.WriteLine($"The length of the password is: {usrPass.Length}");
                    Console.ReadKey();
                }
                else if(usrOper == "-replace")
                {
                    Console.Clear();
                    Console.WriteLine("Replace:");
                    Console.WriteLine();
                    Console.WriteLine($"The current password is: {usrPass}");
                    Console.WriteLine("Please enter a new password:");
                    usrPass = usrPass.Replace(usrPass,Console.ReadLine());
                    Console.WriteLine($"The new password is {usrPass}.");
                    Console.ReadKey();
                }
                else if(usrOper == "-exit")
                {
                    usrExit = true;
                }
                else
                {
                    Console.WriteLine("Invalid command entered.");
                    Console.ReadKey();
                }
            }
            Console.Clear();
            Console.WriteLine("Bye!");
            Console.ReadKey();

        }
    }
}
